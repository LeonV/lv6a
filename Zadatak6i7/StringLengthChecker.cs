﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6i7
{
    class StringLengthChecker : StringChecker
    {
        private int minimumLenght;
        public StringLengthChecker(int minimumLenght) {
            this.minimumLenght = minimumLenght;
        }
        protected override bool PerformCheck(string stringToCheck)
        {
            if (stringToCheck.Length>=minimumLenght)
                return true;
            else
                return false;
        }
    }
}
