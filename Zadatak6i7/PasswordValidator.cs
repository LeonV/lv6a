﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6i7
{
    class PasswordValidator
    {
        private StringChecker firstChecker;
        private StringChecker lastChecker;
        public PasswordValidator(StringChecker firstCheck) {
            firstChecker = firstCheck;
            lastChecker = firstCheck;
        }
        public void AddChecker(StringChecker stringChecker) {

            lastChecker.SetNext(stringChecker);
            lastChecker = stringChecker;
        }
        public bool Check(string stringToCheck)
        {
            return firstChecker.Check(stringToCheck);
        }
    }
}
