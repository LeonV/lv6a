﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6i7
{
    class Program
    {
        static void Main(string[] args)
        {
            string correctString = "OvoJeDobraSifra555";
            string incorrectString = "OvoJeSifraBezBrojeva";
            StringLowerCaseChecker stringLower = new StringLowerCaseChecker();
            StringUpperCaseChecker stringUpper = new StringUpperCaseChecker();
            StringLengthChecker stringLength = new StringLengthChecker(8);
            StringDigitChecker stringDigit = new StringDigitChecker();
            stringLower.SetNext(stringUpper);
            stringUpper.SetNext(stringLength);
            stringLength.SetNext(stringDigit);
            if (stringLower.Check(correctString) == true)
                Console.WriteLine(correctString + " -Zadovoljava sve uvjete");
            else
                Console.WriteLine(incorrectString + " -ne zadovoljava sve uvjete");
            if (stringLower.Check(incorrectString) == true)
                Console.WriteLine(incorrectString + " -Zadovoljava sve uvjete");
            else
                Console.WriteLine(incorrectString + " -ne zadovoljava sve uvjete");

            //7 zadatak:
            Console.WriteLine("Zadatak 7:");
            string goodPassword = "ThisIsGood1234";
            string badPassword = "thisisbad1";
            StringLowerCaseChecker stringLowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker stringUpperCaseChecker = new StringUpperCaseChecker();
            StringLengthChecker stringLengthChecker = new StringLengthChecker(10);
            StringDigitChecker stringDigitChecker = new StringDigitChecker();
            PasswordValidator passwordValidator = new PasswordValidator(stringLengthChecker);
            passwordValidator.AddChecker(stringLowerCaseChecker);
            passwordValidator.AddChecker(stringUpperCaseChecker);
            passwordValidator.AddChecker(stringDigitChecker);
            if (passwordValidator.Check(goodPassword) == true)
                Console.WriteLine(goodPassword + " -Zaporka zadovoljava sve uvjete");
            else
                Console.WriteLine(goodPassword + " -Zaporka ne zadovoljava sve uvjete");
            if (passwordValidator.Check(badPassword) == true)
                Console.WriteLine(badPassword + " -Zaporka zadovoljava sve uvjete");
            else
                Console.WriteLine(badPassword + " -Zaporka ne zadovoljava sve uvjete");
        }
    }
}
