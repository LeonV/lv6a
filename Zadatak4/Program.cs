﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount = new BankAccount("Petar Smith", "Ulica Breza 22", 52691);
            Console.WriteLine(bankAccount.OwnerName + ", Balance:" + bankAccount.Balance);
            Memento memento1 = bankAccount.StoreState();

            bankAccount.UpdateBalance(100000);
            Console.WriteLine(bankAccount.OwnerName + ", Balance:" + bankAccount.Balance);
            bankAccount.RestoreState(memento1);
            Console.WriteLine(bankAccount.OwnerName + ", Balance:" + bankAccount.Balance);

        }
    }
}
