﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class Memento
    {
        public string ownerName { get; private set; }
        public string ownerAddress { get; private set; }
        public decimal balance { get; private set; }
        public Memento(string OwnerName, string OwnerAddress, decimal Balance)
        {
            this.ownerName = OwnerName;
            this.ownerAddress = OwnerAddress;
            this.balance = Balance;
        }
    }
}
