﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product mlijeko = new Product("Svjeze mlijeko", 5.5);
            Product kruh = new Product("Svjezi kruh", 6.5);
            Product banana = new Product("banana", 2.5);
            Box supplybox = new Box();
            supplybox.AddProduct(mlijeko);
            supplybox.AddProduct(kruh);
            supplybox.AddProduct(banana);
            IAbstractIterator iterator = supplybox.GetIterator();
            while (iterator.IsDone == false)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
            }
        }
    }
}
