﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note("LV iz RPPON-a", "Potrebno predati do 23h u cetvrtak.");
            Note note2 = new Note("SiS analiza", "Potrebno predati do subote");
            Note note3 = new Note("Zadaca iz TI", "Potrebno predati do petka");
            Notebook notebook = new Notebook();
            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);
            IAbstractIterator iterator = notebook.GetIterator();
            while (iterator.IsDone == false)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
