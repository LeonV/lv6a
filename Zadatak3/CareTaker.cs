﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class CareTaker
    {
        private List<Memento> PreviousStates;
        public CareTaker() { this.PreviousStates = new List<Memento>();}
        public CareTaker(List<Memento> PreviousStates)
        {
            this.PreviousStates = PreviousStates;
        }
        public void AddPreviousState(Memento PreviousState) {
            PreviousStates.Add(PreviousState);
        }
        public void RemovePreviousState(Memento PreviousState) {
            PreviousStates.Remove(PreviousState);
        }
        public void Clear() {
            PreviousStates.Clear();
        }
        public void Count() {
            PreviousStates.Count();
        }
        public Memento this[int index] { get { return this.PreviousStates[index]; } }

    }
}
